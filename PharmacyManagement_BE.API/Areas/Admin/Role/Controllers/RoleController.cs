﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PharmacyManagement_BE.Application.Commands.RoleFeatures.Requests;
using PharmacyManagement_BE.Application.Queries.RoleFeatures.Requests;

namespace PharmacyManagement_BE.API.Areas.Admin.Role.Controllers
{
    [ApiExplorerSettings(GroupName = "Admin")]
    [Route("api/[controller]")]
    [Area("Admin")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RoleController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Authorize()]
        public async Task<IActionResult> Get()
        {
            try
            {
                var result = _mediator.Send(new GetAllRoleQueryRequest());
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        //[Authorize()]
        public async Task<IActionResult> Post(CreateRoleCommandRequest request)
        {
            try
            {
                var result = _mediator.Send(request);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
